<?php
/*
Projet: MemeSpree 
Description: Site qui permet ajoute des photos
Version: 1
*/
require_once './php/fonctions.images.php';
require_once './php/dataBase.php';

session_start();
if (!isset($_SESSION['pseudo'])) {
    $_SESSION['pseudo'] = "";
    $_SESSION['typeDeTri'] = 7;
}
$pseudo = $_SESSION['pseudo'];
if (isset(ReadUserByPseudo($pseudo)[0])){
$idUser = ReadUserByPseudo($pseudo)[0]["idUser"];
}
else{
    $idUser = 0;
}

$btn = filter_input(INPUT_POST, 'like');
if ($btn>0){
    if ($idUser != 0){
        if (isset(ReadPhotoById($btn)[0])){
            if (isset(ReadlikeByIdUserIdPhoto($idUser, $btn)[0])){
                DeleLike($idUser, $btn);
            }
            else{
                AddLike($idUser, $btn);
            }
        }
    }
    else{
        header('location: ./signin.php');
    }
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper black">
                <a href="#" class="brand-logo center " style=" padding-left: 250px;"><img src="image/feuille.png" alt="Logo_race" style="width: 50px; height: 50px; "></a>
                <a href="#!" class="brand-logo center">memeSpree</a>
                <ul class="left hide-on-med-and-down">
                    <li><a href="./index.php">Home</a></li>
                    <li><a href="./galerie.php">Gallery</a></li>
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signin.php">Connexion</a></li>
                    <?php } ?>
                        
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signup.php">Inscription</a></li>
                    <?php } ?>

                    <?php if ($_SESSION['pseudo'] != "") { ?>
                        <li><a href="./logout.php">logout</a></li>
                    <?php } ?>
                        <?php if ($_SESSION['pseudo'] != "") { ?>
                        <li><a href="./addPhoto.php">Ajouter des photos</a></li>
                    <?php } ?>
                </ul>
                <?php
                require_once './nav.php';
                if ($_SESSION['pseudo'] != "") {
                    echo $nav;
                }
                ?>
            </div>
        </nav>
        <h1 id="post">Vos publications</h1>
        <form method="POST" action="">
            <div class="row">
                <div class="col-xs-12">

                    <div class="card">
                        <?php echo showImagesById($idUser); ?>
                    </div> 
                </div>
            </div>
        </form>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.slider').slider();
                $('input#input_text, textarea#textarea2').characterCounter();
            });
        </script>
        <footer id="Footer">
            <?php
            require_once './footer.php';
            echo $footer;
            ?>
        </footer>
    </body>
</html>

