<!--
Projet: MemeSpree
Description: Site qui permet ajoute des photos
Version: 1
Auteur: Christian Russo 
-->
<?php
require_once("./php/dataBase.php");
session_start();
if (!isset($_SESSION['logUser'])) {
    $_SESSION['pseudo'] = "";
    $_SESSION['typeDeTri'] = 7;
}

if ($_SESSION['pseudo'] != "") {
    header('location: galerie.php');
}

$pseudo = filter_input(INPUT_POST, 'nickname', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$error = "";

//Se connecter au site
if (isset($_POST["connecter"])) {
    if (isset(ReadUserByPseudo($pseudo)[0]["pseudo"])) {
        $data = ReadUserByPseudo($pseudo)[0];
        if (sha1($password . $data["salt"]) == $data["pwd"]) {
            $_SESSION['pseudo'] = $pseudo;
            header('location: ./galerie.php');
            exit;
        }
    } else {
        $error .= "L'utilisateur n'existe pas";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Connexion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--css main du site web-->
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper black">

                <a href="#" class="brand-logo center " style=" padding-left: 250px;"><img src="image/feuille.png" alt="Logo_race" style="width: 50px; height: 50px; "></a>
                <a href="#!" class="brand-logo center">memeSpree</a>

                <ul class="left hide-on-med-and-down">
                    <li><a href="./index.php">Home</a></li>
                    <li><a href="./galerie.php">Gallery</a></li>
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signup.php">Inscription</a></li>
                    <?php } ?>
                </ul>
                <?php
                require_once './nav.php';
                if ($_SESSION['pseudo'] != "") {
                    echo $nav;
                }
                ?>
            </div>

        </nav>
        <form method="POST" action="#">
            <div class="container">
                <h1>Connexion</h1>
                <div class="card horizontal">                          
                    <div class="card-stacked">
                        <div class="card-content">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="icon_prefix" name="nickname" type="text" class="validate" placeholder="Username">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="icon_lock" name="password" type="password" class="validate" placeholder="Password">
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="connecter">Se connecter
                                <i class="material-icons right">send</i>
                            </button>
                            <div class="collection">
                                <a href="signup.php" class="collection-item"><span class="badge"></span><?php echo "Vous n'êtes pas inscrit?" ?></a>
                            </div>
                            <?php
                            if ($error != "") {
                                echo "<h4>$error</h4>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php require_once './footer.php'; ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('input#input_text, textarea#textarea2').characterCounter();
            });
        </script>

    </body>
</html>
