<?php
/*
Projet: MemeSpree 
Description: Site qui permet ajoute des photos
Version: 1
*/
session_start();
if (!isset($_SESSION['pseudo'])) {
    $_SESSION['pseudo'] = "";
    $_SESSION['typeDeTri'] = 7;
}
$pseudo = $_SESSION['pseudo'];
$typeDeTri = $_SESSION['typeDeTri'];

$temp_typeDeTri = filter_input(INPUT_GET, 'type');
if($temp_typeDeTri == 7 || $temp_typeDeTri == 30 || $temp_typeDeTri == 365 ){
    $_SESSION['typeDeTri'] = $temp_typeDeTri;
    header('location: galerie.php');
    exit;
}

require_once './php/fonctions.images.php';
require_once './php/dataBase.php';
require_once './nav.php';

if (isset(ReadUserByPseudo($pseudo)[0])){
$idUser = ReadUserByPseudo($pseudo)[0]["idUser"];
}
else{
    $idUser = 0;
}

$btn = filter_input(INPUT_POST, 'like');
if ($btn>0){
    if ($idUser != 0){
        if (isset(ReadPhotoById($btn)[0])){
            if (isset(ReadlikeByIdUserIdPhoto($idUser, $btn)[0])){
                DeleLike($idUser, $btn);
            }
            else{
                AddLike($idUser, $btn);
            }
        }
    }
    else{
        header('location: ./signin.php');
    }
}

switch ($typeDeTri){
    case 7 :
        $data = CountLikeByIdPhotoOn7Day();
    break;
    case 30 :
        $data = CountLikeByIdPhotoOn30Day();
    break;
    case 365 :
    $data = CountLikeByIdPhotoOn365Day();
    break;
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper black">
                <a href="#" class="brand-logo center " style=" padding-left: 250px;"><img src="image/feuille.png" alt="Logo_race" style="width: 50px; height: 50px; "></a>
                <a href="#!" class="brand-logo center">memeSpree</a>
                <ul class="left hide-on-med-and-down">
                    <li><a href="./index.php">Home</a></li> 
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signin.php">Connexion</a></li>
                        <li><a href="./signup.php">Inscription</a></li>
                    <?php } ?>
                        <?php if ($_SESSION['pseudo'] != "") { ?>
                        <li><a href="./logout.php">logout</a></li>
                        <li><a href="./addPhoto.php">Ajouter des photos</a></li>
                        <li><a><?=$nav?></a></li>
                        
                    <?php } ?>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li>Photo les plus like sous :</li>
                    <li><a href="galerie.php?type=7">7 jours</a></li>
                    <li><a href="galerie.php?type=30">30 jours</a></li>
                    <li><a href="galerie.php?type=365">365 jours</a></li>
                </ul>
            </div>
        </nav>
        <form method="POST" action="">
            <div class="row">
                <div class="col-xs-12">

                    <div class="card">
                        <?php echo showImages($idUser, $data); ?>
                    </div> 
                </div>
            </div>
        </form>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.slider').slider();
                $('input#input_text, textarea#textarea2').characterCounter();
            });
        </script>
        <footer id="Footer">
            <?php
            require_once './footer.php';
            echo $footer;
            ?>
        </footer>
    </body>
</html>

