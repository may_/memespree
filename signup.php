<?php
//Projet: memeSpree
//Description: Site qui permet ajoute des photos
//Version: 1
//Auteur: Christian Russo 
//Connexion à la base de données
session_start();

if (!isset($_SESSION['logUser']) || !isset($_SESSION['logUser'])) {
    $_SESSION['logUser'] = ['logged' => FALSE];
    $_SESSION['pseudo'] = "";
    $_SESSION['typeDeTri'] = 7;
}

if ($_SESSION['pseudo'] != "") {
    header('location: galerie.php');
}

require_once("./php/dataBase.php");


connexion();
$error = "";


if (isset($_POST["Envoyer"])) {
    $nickname = filter_input(INPUT_POST, 'nickname', FILTER_SANITIZE_STRING);
    $pwd = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $salt = generateRandomString(20);
    $error = "";
    if ($nickname == "") {
        $error .= "L'username est obligatoire<br>";
    }
    if ($pwd == "") {
        $error .= "Le mot de passe est obligatoire<br>";
    }
    if ($email == "") {
        $error .= "L'email est obligatoire<br>";
    }
    if (ReadUserByPseudo($nickname) != null) {
        $error .= "L'utilisateur existe déjà";
    }
    if ($error == "") {
        AddUser($nickname, $email, sha1($pwd . $salt), $salt);
        header('location: ./signin.php');
        exit;
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inscription</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--css main du site web-->
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper black">
                <a href="#" class="brand-logo center " style=" padding-left: 250px;"><img src="image/feuille.png" alt="Logo_race" style="width: 50px; height: 50px; "></a>
                <a href="#!" class="brand-logo center">memeSpree</a>
                <ul class="left hide-on-med-and-down">
                    <li><a href="./index.php">Home</a></li>
                    <li><a href="./galerie.php">Gallery</a></li>
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signin.php">Connexion</a></li>
                    <?php } ?>

                    <?php if ($_SESSION['pseudo'] != "") { ?>
                        <li><a href="./logout.php">logout</a></li>
                    <?php } ?>
                </ul>
                <?php
                require_once './nav.php';
                if ($_SESSION['pseudo'] != "") {
                    echo $nav;
                }
                ?>
            </div>
        </nav>
        <form method="POST" action="#">
            <div class="container">
                <h1>Inscription</h1>
                <div class="card horizontal">                          
                    <div class="card-stacked">
                        <div class="card-content">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="icon_prefix" name="nickname" type="text" class="validate" required="required" placeholder="Username">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="icon_lock" name="password" type="password" class="validate" required="required" placeholder="Password">   
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="email" name="email" class="validate" required="required" placeholder="Email">                           
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="Envoyer">Envoyer</button>
                        </div>
                    </div>
                </div>
                <?php
                if ($error != "") {
                    echo "<h4>$error</h4>";
                }
                ?>
            </div>
        </form>
        <?php require_once './footer.php'; ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('input#input_text, textarea#textarea2').characterCounter();
            });
        </script>

    </body>
</html>
<?php
    function generateRandomString($nbLetters) {
        $randString = "";
        $charUniverse = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i = 0; $i < $nbLetters; $i++) {
            $randInt = rand(0, 61);
            $randChar = $charUniverse[$randInt];
            $randString = $randString .= $randChar;
        }
        return $randString;
    }
?>