 <!--
Projet: memeSpree
Description: Site qui permet ajoute des photos
Version: 1
Auteur: Christian Russo 
-->
<?php
//Connexion à la base de données
session_start();
require_once("./php/dataBase.php");
$error = "";

if (!isset($_SESSION["pseudo"])){
    header("Location: index.php");
    exit;
}
elseif($_SESSION["pseudo"] == "") {
    header("Location: index.php");
    exit;
}


if (isset($_POST["Envoyer"])) {

    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $user = $_SESSION["pseudo"];
    $fileToUpload = filter_input(INPUT_POST, "fileToUpload");
    
    $target_dir = "image/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    if (!isset(ReadPhotoByChemin($_FILES["fileToUpload"]["name"])[0])){
            $uploadOk = 1;

            if($imageFileType != "png" && $imageFileType != "jpg" ) {
            $error .= "L'image doit être en format png ou jpg";
            $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
            $error .= "Votre image n'a pas pu être mis en ligne";
            // if everything is ok, try to upload file
            }
            else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                // file upload
                addPhoto($_FILES["fileToUpload"]["name"], $description, readUserByPseudo($user)[0]["idUser"]);
                header("Location: galerie.php");
                exit;
            }
            else {
            // l'uplode n'a pas marcher
            $error .= "Votre image n'a pas pu être mis en ligne";
            }
        }
    }
    $error .= "Votre image n'a pas pu être mis en ligne car elle existe deja sur le site";    
}


?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ajouter une photo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!--css main du site web-->
        <link rel="stylesheet" href="./style.css">
    </head>
    <body>
        <nav>
            <div class="nav-wrapper black">
                <a href="#" class="brand-logo center " style=" padding-left: 250px;"><img src="image/feuille.png" alt="Logo_race" style="width: 50px; height: 50px; "></a>
                <a href="#!" class="brand-logo center">memeSpree</a>
                <ul class="left hide-on-med-and-down">
                <li><a href="./index.php">Home</a></li>
                <li><a href="./galerie.php">Gallery</a></li>
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signin.php">Connexion</a></li>
                    <?php } ?>
                        
                    <?php if ($_SESSION['pseudo'] == "") { ?>
                        <li><a href="./signup.php">Inscription</a></li>
                    <?php } ?>

                    <?php if ($_SESSION['pseudo'] != "") { ?>
                        <li><a href="./logout.php">logout</a></li>
                    <?php } ?>
                </ul>
                <?php
                require_once './nav.php';
                if ($_SESSION['pseudo'] != "") {
                    echo $nav;
                }
                ?>
                </ul>
            </div>
        </nav>
        <form method="POST" action="#" enctype="multipart/form-data">
            <div class="container">
                <h1>Ajouter une photo</h1>
                <div class="card horizontal">                          
                    <div class="card-stacked">
                        <div class="card-content">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="icon_lock" name="description" type="text" class="validate"  placeholder="Description">   
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                <input type="file" name="fileToUpload" id="fileToUpload">                           
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="Envoyer">Envoyer</button>
                        </div>
                    </div>
                </div>
                <?php if($error != "") {echo "<h4>$error</h4>";} ?>
            </div>
        </form>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('input#input_text, textarea#textarea2').characterCounter();
                $('.modal').modal();
            });
        </script>
    </body>
</html>