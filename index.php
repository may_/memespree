<!--
Projet: MemeSpree 
Description: Site qui permet ajoute des photos
Version: 1
Auteurs: Mayara Ccochard, Christian Russo, Lucas Landrescy  et Samuel Bongard
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MemeSpree</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <div class="slider fullscreen">
            <ul class="slides fullscreen" >
                <li>
                    <img src="image/03.jpg" >
                    <div class="caption center-align">
                        <p>
                        <h3>Bienvenue sur MemeSpree
                           
                        </h3>
                         <img src="Images/28c5bdd48a23a6e735bb93532e4f53ce.png" alt="Logo_race" style="width: 70px; height: 70px; ">    
                    </p>
                        <h5 id="textAccueil">Montrez votre créativité à travers vos photos</h5>
                    </div>
                </li>
                <li>
                    <img src="image/07.jpg"> <!-- random image -->
                    <div class="caption left-align">
                        <h3>Accès à la galerie</h3>
                        <h5 class="light grey-text text-lighten-3"><a href="galerie.php" class="waves-effect waves-light btn-small">Galerie</a></h5>
                    </div>
                </li>
                <li>
                    <img src="image/04.jpg">
                    <div class="caption right-align">
                        <h3>Inscrivez-vous pour publier vos photos</h3>
                        <h5 class="light black-text text-lighten-3"><a href="signup.php" class="waves-effect waves-light btn-small">S'inscrire</a></h5>
                    </div>
                </li>
                <li>
                    <img src="image/1_vOrfclMVms7S4608zxC-ig.jpg">
                    <div class="caption right-align">
                        <h3>Connectez-vous pour publier vos photos</h3>
                        <h5 class="light black-text text-lighten-3"><a href="signin.php" class="waves-effect waves-light btn-small">Se connecter</a></h5>
                    </div>
                </li>
            </ul>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.slider').slider();
            });
        </script>
</html>
