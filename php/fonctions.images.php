<?php
require_once 'dataBase.php';

function showImages($idUserWatch, $data) {

    $tab = $data;
    $html = "";
    for ($i = 0; $i < count($tab); $i++) {
        $tab2 = ReadUserById($tab[$i]['idUser']);

        // TRAITEMENT AFFICHAGE
        $html .= "<div class='card col xl4 s12'>";
        $html .= "<div class='card-image'>";
        $html .= "<img src='image/" . $tab[$i]['chemin'] . "' alt=" . $tab[$i]['chemin'] . " style='height:350px;>";
        $html .= "</div>";
        $html .= "<div class = 'card-content'>";
        $html .= "<p>Pseudo: " . $tab2[0]['pseudo'] . "</p>";
        $html .= "<button type='submit' name='like' value='".$tab[$i]['idPhoto']."'><img style='height:50px;width:50px' src='like/".
                    (isset(ReadlikeByIdUserIdPhoto($idUserWatch, $tab[$i]['idPhoto'])[0]['idUser'])? "like.png":"not-like.png")                    
                    ."' alt='Image de like ou pas'>".ReadLikeByIdPhoto($tab[$i]['idPhoto'])[0]["Nb_like"]."</button>";
        $html .= "<p>" . $tab[$i]['description'] . "</p>";
        $html .= "</div>";
        $html .= "</div>";
        
    }
    return $html;
}

function showImagesById($idUserWatch) {

    $tab = ReadPhotoByIdUser($idUserWatch);
    $tab2 = ReadUserById($idUserWatch);
    $html = "";
    for ($i = 0; $i < count($tab); $i++) {
        
        // TRAITEMENT AFFICHAGE
        $html .= "<div class='card col xl4 s12'>";
            $html .= "<div class='card-image'>";
                $html .= "<img src='image/" . $tab[$i]['chemin'] . "' alt=" . $tab[$i]['chemin'] . " style='height:350px;>";
                $html .= "</div>";
                $html .= "<div class = 'card-content'>";
                $html .= "<p>Pseudo: " . $tab2[0]['pseudo'] . "</p>";
                $html .= "<button type='submit' name='like' value='".$tab[$i]['idPhoto']."'><img style='height:50px;width:50px' src='like/".
                            (isset(ReadlikeByIdUserIdPhoto($idUserWatch, $tab[$i]['idPhoto'])[0]['idUser'])? "like.png":"not-like.png")                    
                            ."' alt='Image de like ou pas'>".ReadLikeByIdPhoto($tab[$i]['idPhoto'])[0]["Nb_like"]."</button>";
                $html .= "<p>" . $tab[$i]['description'] . "</p>";
            $html .= "</div>";
        $html .= "</div>";
    }
    return $html;
}
