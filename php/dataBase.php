<?php 
/*define("HOST", "8h05f.myd.infomaniak.com");
define("DBNAME", "memeSpree");
define("DBUSER", "8h05f_memespree");
define("DBPWD", "Spreememe123!");*/

define("HOST", "localhost");
define("DBNAME", "memespree");
define("DBUSER", "root");
define("DBPWD", "");

function connexion() {
  static $dbc = null;

  // Première visite de la fonction
  if ($dbc == null) {
    // Essaie le code ci-dessous
    try {
      $dbc = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME, DBUSER, DBPWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_PERSISTENT => true));
        $dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    // Si une exception est arrivée
    catch (Exception $e) {
      echo 'Erreur : ' . $e->getMessage() . '<br />';
      echo 'N° : ' . $e->getCode();
      // Quitte le script et meurt
      die('Could not connect to MySQL');
    }
  }
  // Pas d'erreur, retourne un connecteur
  return $dbc;
}

function Insert($sql, $data){
  try{
      $query = connexion()->prepare($sql);

      $query->execute($data);
  }
  catch (Exception $e) {
      die("Impossible de se connecter à la base ". $e->getMessage());
  }
}

function Select($sql, $data){
  try{
      $query = connexion()->prepare($sql);

      $query->execute($data);
      return $query->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (Exception $e) {
      die("Impossible de se connecter à la base ". $e->getMessage());
  }
}

// USER
function AddUser($pseudo, $email, $pwd, $salt){
  $sql = "INSERT INTO `user`(`pseudo`, `email`, `pwd`, `salt`)
  VALUES (:pseudo, :email, :pwd, :salt)";
  
  $data=[":pseudo"=> $pseudo, 
  ":email"=> $email, 
  ":pwd"=> $pwd, 
  ":salt"=> $salt];

  Insert($sql, $data);
}

function ReadAllUser(){
  $sql = "SELECT `idUser`, `pseudo`, `email`, `pwd`, `salt` FROM `user`";
  $data = [];

  $result = Select($sql, $data);
  return $result;
}

function ReadUserById($id){
  $sql = "SELECT `idUser`, `pseudo`, `email`, `pwd`, `salt` FROM `user` WHERE `idUser` = :id";
  $data = [":id"=>$id];

  $result = Select($sql, $data);
  return $result;
}

function ReadUserByLogin($pseudo, $pwd){
  $sql = "SELECT `idUser`, `pseudo`, `email`, `pwd`, `salt` 
  FROM `user`
  WHERE `pseudo` = :pseudo 
  and `pwd` = :pwd" ;

  $data = [":pseudo"=>$pseudo, ":pwd"=>$pwd];

  $result = Select($sql, $data);
  return $result;
}

function ReadUserByPseudo($pseudo){
  $sql = "SELECT `idUser`, `pseudo`, `email`, `pwd`, `salt` 
  FROM `user` 
  WHERE `pseudo` = :pseudo";

  $data = [":pseudo"=>$pseudo];

  $result = Select($sql, $data);
  return $result;
}

function ReadUserByEmail($email){
  $sql = "SELECT `idUser`, `pseudo`, `email`, `pwd`, `salt` 
  FROM `user`  
  WHERE `email` = :email";

  $data = [":email"=>$email];

  $result = Select($sql, $data);

  return $result;

}

// PHOTO
function AddPhoto($chemin, $description, $idUser) {
  $sql = "INSERT INTO `photo`(`chemin`, `description`, `idUser`)
  VALUES (:chemin, :description, :idUser)";

  $data = [":chemin" => $chemin,
      ":description" => $description,
      ":idUser" => $idUser];

  Insert($sql, $data);
}

function ReadAllPhoto() {
  $sql = "SELECT `idPhoto`, `chemin`, `description`, `idUser` FROM `photo`";
  $data = [];

  $result = Select($sql, $data);
  return $result;
}

function ReadPhotoById($id) {
  $sql = "SELECT `idPhoto`, `chemin`, `description`, `idUser` FROM `photo` WHERE `idPhoto` = :id";
  $data = [":id" => $id];

  $result = Select($sql, $data);
  return $result;
}

function ReadPhotoByIdUser($id) {
  $sql = "SELECT `idPhoto`, `chemin`, `description`, `idUser` FROM `photo` WHERE `idUser` = :id";
  $data = [":id" => $id];

  $result = Select($sql, $data);
  return $result;
}

function ReadPhotoByChemin($chemin) {
  $sql = "SELECT `idPhoto`, `chemin`, `description`, `idUser` FROM `photo`
 WHERE `description` LIKE ':chemin'";

  $data = [":chemin" => $chemin];

  $result = Select($sql, $data);
  return $result;
}

// LIKE
function AddLike($idUser, $idPhoto) {
  $sql = "INSERT INTO `like`(`idUser`, `idPhoto`, `date`)
 VALUES (:idUser, :idPhoto, Now())";

  $data = [":idUser" => $idUser,
      ":idPhoto" => $idPhoto];

  Insert($sql, $data);
}

function ReadAllLike() {
  $sql = "SELECT `idUser`, `idPhoto`, `date` FROM `like`";
  $data = [];

  $result = Select($sql, $data);
  return $result;
}

function ReadlikeByIdUserIdPhoto($idUser, $idPhoto) {
  $sql = "SELECT `idUser`, `idPhoto`, `date` FROM `like` WHERE `idPhoto` = :idPhoto AND `idUser` = :idUser";
  $data = [":idPhoto" => $idPhoto, ":idUser" => $idUser];

  $result = Select($sql, $data);
  return $result;
}

function ReadLikeByIdUser($id) {
  $sql = "SELECT `idUser`, `idPhoto`, `date` FROM `like` WHERE `idUser` = :id";
  $data = [":id" => $id];

  $result = Select($sql, $data);
  return $result;
}

function ReadLikeByIdPhoto($id) {
  $sql = "SELECT `idUser`, `idPhoto`, `date`, count(*) as 'Nb_like' FROM `like` WHERE `idPhoto` = :id";
  $data = [":id" => $id];

  $result = Select($sql, $data);
  return $result;
}

function DeleLike($idUser, $idPhoto) {
  $sql = "DELETE FROM `like` WHERE `idUser` = :idUser and `idPhoto`= :idPhoto ";

  $data = [":idUser" => $idUser,
      ":idPhoto" => $idPhoto];

  Insert($sql, $data);
}

// NB LIKE
function CountLikeByIdPhotoOn7Day(){
  $sql = "SELECT count(*) AS 'Nb_Like', `photo`.`idPhoto`, `photo`.`chemin`, `photo`.`description`, `photo`.`idUser` FROM`photo`
          LEFT JOIN `like` on `like`.`idPhoto` = `photo`.`idPhoto`
          AND `like`.`date` BETWEEN (NOW()-INTERVAL 7 DAY) and Now()
          GROUP BY idPhoto
          ORDER BY 'Nb_Like' desc";
  $data = [];

  $result = Select($sql, $data);
  return $result;
}

function CountLikeByIdPhotoOn30Day(){
  $sql = "SELECT count(*) AS 'Nb_Like', `photo`.`idPhoto`, `photo`.`chemin`, `photo`.`description`, `photo`.`idUser` FROM  `photo`
          LEFT JOIN `like` on `like`.`idPhoto` = `photo`.`idPhoto`
          AND `like`.`date` BETWEEN (NOW()-INTERVAL 1 MONTH) and Now()
          GROUP BY idPhoto
          ORDER BY 'Nb_Like' desc";
  $data = [];

  $result = Select($sql, $data);
  return $result;
}

function CountLikeByIdPhotoOn365Day(){
  $sql = "SELECT count(*) AS 'Nb_Like', `photo`.`idPhoto`, `photo`.`chemin`, `photo`.`description`, `photo`.`idUser` FROM `photo`
          LEFT JOIN `like` on `like`.`idPhoto` = `photo`.`idPhoto`
          AND `like`.`date` BETWEEN (NOW()-INTERVAL 1 YEAR) and Now()
          GROUP BY idPhoto
          ORDER BY 'Nb_Like' desc";
  $data = [];

  $result = Select($sql, $data);
  return $result;
}