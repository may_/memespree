-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema memeSpree
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema memeSpree
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `memeSpree` DEFAULT CHARACTER SET utf8 ;
USE `memeSpree` ;

-- -----------------------------------------------------
-- Table `memeSpree`.`USER`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `USER` (
  `idUser` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pseudo` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `pwd` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE INDEX `idUser_UNIQUE` (`idUser` ASC) ,
  UNIQUE INDEX `pseudo_UNIQUE` (`pseudo` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memeSpree`.`PHOTO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PHOTO` (
  `idPhoto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `chemin` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `idUser` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idPhoto`),
  UNIQUE INDEX `chemin_UNIQUE` (`chemin` ASC) ,
  UNIQUE INDEX `idPhoto_UNIQUE` (`idPhoto` ASC) ,
  INDEX `fk_PHOTO_USER_idx` (`idUser` ASC) ,
  CONSTRAINT `fk_PHOTO_USER`
    FOREIGN KEY (`idUser`)
    REFERENCES `USER` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memeSpree`.`LIKE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LIKE` (
  `idUser` INT UNSIGNED NOT NULL,
  `idPhoto` INT UNSIGNED NOT NULL,
  `date` DATETIME NOT NULL,
  INDEX `fk_LIKE_USER1_idx` (`idUser` ASC) ,
  INDEX `fk_LIKE_PHOTO1_idx` (`idPhoto` ASC) ,
  PRIMARY KEY (`idUser`, `idPhoto`),
  CONSTRAINT `fk_LIKE_USER1`
    FOREIGN KEY (`idUser`)
    REFERENCES `USER` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_LIKE_PHOTO1`
    FOREIGN KEY (`idPhoto`)
    REFERENCES `PHOTO` (`idPhoto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
